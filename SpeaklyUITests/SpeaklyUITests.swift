//
//  iOSTemplateUITests.swift
//  iOSTemplateUITests
//
//  Created by John Hampton on 2/24/19.
//

import XCTest

class SpeaklyUITests: XCTestCase {
    let app = XCUIApplication()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        app.launchArguments += ["-hasViewOnboarding", "false"]
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        app.terminate()
    }

    func testShowOnboardingAndNavigateToLogin() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        app.buttons["Next"].tap()
        app.buttons["Next"].tap()
        app.buttons["Get Started"].tap()
        XCTAssertTrue(app.staticTexts["Need an account?"].exists)
    }
    
    func testShowSignUp() {
        app.buttons["Skip"].tap()
        XCTAssertTrue(app.staticTexts["Need an account?"].exists)
        app.buttons["Register"].tap()
        XCTAssertTrue(app.staticTexts["Already have an account?"].exists)
    }

}
