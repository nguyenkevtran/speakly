//
//  iOSTemplateTests.swift
//  iOSTemplateTests
//
//  Created by John Hampton on 2/24/19.
//

import XCTest
@testable import Speakly

class UserDataStoreTests: XCTestCase {
    
    private let userDataStore = UserDataStore.instance
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        userDataStore.email = nil
        userDataStore.username = nil
        userDataStore.uid = nil
    }
    
    func testStartWithAllNilFields() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssert(userDataStore.email == nil)
        XCTAssert(userDataStore.username == nil)
        XCTAssert(userDataStore.uid == nil)
    }
    
    func testShouldHaveCorrectValues() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        userDataStore.email = "giangp@metropolia.fi"
        userDataStore.username = "giangp"
        userDataStore.uid = "afgghughudehgdushugerueiurgdeggmskafaq[woq"
        XCTAssert(userDataStore.email == "giangp@metropolia.fi")
        XCTAssert(userDataStore.username == "giangp")
        XCTAssert(userDataStore.uid == "afgghughudehgdushugerueiurgdeggmskafaq[woq")
    }
}
