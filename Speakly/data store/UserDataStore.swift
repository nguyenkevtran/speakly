//
//  UserDataStore.swift
//  Speakly
//  Store user information locally 
//  Created by Giang Pham on 04/05/2019.
//

import Foundation

class UserDataStore {
    static let instance = UserDataStore()
    
    private init(){}
    
    var uid : String?
    
    var username: String?
    
    var email: String?
    
    var language: String?
    
    var level: Int?
    
    var phone: String?
    
    var courses: [Int]?
}
