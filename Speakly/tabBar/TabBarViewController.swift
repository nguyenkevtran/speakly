//
//  TabBarViewController.swift
//  Speakly
//  TabBarView swift file for setting up app's tabbar
//  Created by Phat Doan on 29/04/2019.
//

import UIKit
import Localize_Swift

class TabBarViewController: UITabBarController {
    
    static func storyboardInstance() -> TabBarViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? TabBarViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
