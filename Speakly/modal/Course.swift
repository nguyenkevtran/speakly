//
//  Course.swift
//  Speakly
//  Data model for course (properties and init function)
//  Created by iosdev on 30/04/2019.
//

import Foundation
class Course {
    var id: Int
    var image: String
    var title: String
    var author: String
    var price: String
    var attendantCount = 0
    var description: String
    var goals: [String]
    var ratings: Double
    var level: Int
    var url: String
    
    init(_ dictionary: Dictionary<String, AnyObject>, _ id: Int?) {
        self.id = id!
        self.image = (dictionary["image"] as! NSString) as String
        self.title = (dictionary["title"] as! NSString) as String
        self.author = (dictionary["author"] as! NSString) as String
        self.price = (dictionary["price"] as! NSString) as String
        self.attendantCount = dictionary["attendantCount"] as! NSInteger
        self.description = (dictionary["description"] as! NSString) as String
        self.goals = (dictionary["goals"] as! NSArray) as! [String]
        self.ratings = (dictionary["ratings"] as! NSNumber) as! Double
        self.level = (dictionary["level"] as! NSInteger) as Int
        self.url = (dictionary["url"] ?? "" as AnyObject)! as! String
    }
}
