//
//  LoginViewController.swift
//  iOSTemplate
//  Log in view page' logic
//  Created by Giang Pham on 20/04/2019.
//

import UIKit
import Firebase

class LoginViewController: BaseViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var errorMessage: UILabel!
    static func storyboardInstance() -> LoginViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? LoginViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        login.contentEdgeInsets = UIEdgeInsets(top: 10.0, left: 0.0, bottom: 10.0, right: 0.0)
    }
    @IBAction func login(_ sender: UIButton) {
        guard let email = email?.text, let password = password?.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (authData, err) in
            if (err != nil) {
                print("ERROR: Fail to login user", err!)
            }
        }
    }
    
    @IBAction func register(_ sender: UIButton) {
        let signupVC = SignUpViewController.storyboardInstance()!
        present(signupVC, animated: true, completion: nil)
    }
    
    @IBAction func emailChanged(_ sender: UITextField) {
        login.isEnabled = !(email.text?.isEmpty ?? true || password.text?.isEmpty ?? true)
        errorMessage.isHidden = true
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
        login.isEnabled = !(email.text?.isEmpty ?? true || password.text?.isEmpty ?? true)
        errorMessage.isHidden = true
    }
}
