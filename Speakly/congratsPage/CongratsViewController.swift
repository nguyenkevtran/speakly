//
//  CongratsViewController.swift
//  Speakly
//  CongratsView for congratulation page after enroll successfully
//  Created by Phat Doan on 04/05/2019.
//

import UIKit
import Localize_Swift

class CongratsViewController: UIViewController {
    
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var subCongratsLabel: UILabel!
    @IBOutlet weak var startTheCourseButton: UIButton!
    @IBOutlet weak var backToHomeButton: UIButton!
    var urlString = ""
    
    static func storyboardInstance() -> CongratsViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? CongratsViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        setText()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    // set text for UI
    @objc func setText() {()
        congratsLabel.text = "Congratulations!".localized()
        subCongratsLabel.text = "You have enrolled a new course".localized()
        startTheCourseButton.setTitle("START THE COURSE".localized(), for: .normal)
        backToHomeButton.setTitle("Back to home".localized(), for: .normal)
    }
    
    // update UI shadow and radius
    func updateUI() {
        startTheCourseButton.layer.cornerRadius = 10
        startTheCourseButton.layer.shadowOffset = .zero
        startTheCourseButton.layer.shadowRadius = 5
        startTheCourseButton.layer.shadowOpacity = 0.1
        startTheCourseButton.layer.shadowColor = UIColor.black.cgColor
    }

    // get start course button
    @IBAction func startTheCourseTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebKitViewController") as? WebKitViewController
        vc?.urlString = urlString
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor(red: 74/255, green: 21/255, blue: 75/255, alpha: 1)
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    // back to home button
    @IBAction func backToHomeTapped(_ sender: Any) {
        let homeVC = TabBarViewController.storyboardInstance()!
        present(homeVC, animated: true, completion: nil)
    }
}
