//
//  DetailViewController.swift
//  Speakly
//  DetailView swift file to handle all of the logic of detail page
//  Created by iosdev on 04/05/2019.
//

import UIKit
import Cosmos
import Firebase

class DetailViewController: UIViewController {

    @IBOutlet weak var course_image: UIImageView!
    @IBOutlet weak var course_title: UILabel!
    @IBOutlet weak var course_author: UILabel!
    @IBOutlet weak var course_rating: CosmosView!
    @IBOutlet weak var course_price: UILabel!
    @IBOutlet weak var course_description: UILabel!
    @IBOutlet weak var course_requirement: UILabel!
    @IBOutlet weak var goalsView: UIView!
    @IBOutlet weak var enroll_button: UIButton!
    @IBOutlet weak var continue_button: UIButton!
    @IBOutlet weak var top_right_nav_button: UIBarButtonItem!
    
    var course: Course? = nil
    var courseGoal: [String] = []
    var urlString = ""
    var firebaseRef: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseRef = Database.database().reference()
        enroll_button.layer.cornerRadius = 10
        continue_button.layer.cornerRadius = 10
        // display detail page UI if course is not nil
        if (course != nil) {
            course_title.text = course?.title
            course_author.text = course?.author
            course_rating.rating = Double(course?.ratings ?? 0)
            course_price.text = course?.price
            course_description.text = course?.description
            course_requirement.text = getLevelString(level: course?.level ?? 1)
            courseGoal = course?.goals ?? []
            course_image.load(url: URL(string: (course?.image)!)!)
            urlString = course?.url ?? ""
            course_image.layer.masksToBounds = true
            course_image.layer.cornerRadius = 10
            createGoals()
            let myCourses = UserDataStore.instance.courses
            // logic for displaying enroll or continue button for course
            if (UserDataStore.instance.uid == nil || (myCourses?.contains((course!.id)) ?? false) ) {
                enroll_button.isHidden = true;
                enroll_button.isEnabled = false;
                enroll_button.isUserInteractionEnabled = false;
                if (myCourses?.contains((course!.id))) == true {
                    top_right_nav_button.title = "Continue"
                    continue_button.isHidden = false;
                    continue_button.isEnabled = true;
                    continue_button.isUserInteractionEnabled = true;
                }
            }
        }
    }
    
    // top right nav button is clicked
    @IBAction func topRightNavButtonClicked(_ sender: Any) {
        let myCourses = UserDataStore.instance.courses
        if (myCourses?.contains((course!.id))) == true {
            continueCourse()
        } else {
            enrollCourse()
        }
    }
    
    // bottom continue button is clicked
    @IBAction func continueCourse(_ sender: UIButton) {
        continueCourse()
    }
    
    // bottom enroll button is clicked
    @IBAction func enrollCourse(_ sender: UIButton) {
        enrollCourse()
    }
    
    // create course goal programmatically
    func createGoals() {
        var viewArray = [UILabel]()
        for (index, goal) in courseGoal.enumerated() {
            var margins = goalsView.layoutMarginsGuide
            let label = UILabel()
            label.numberOfLines = 0
            label.translatesAutoresizingMaskIntoConstraints = false
            self.goalsView.addSubview(label)
            label.text = goal
            label.textColor = UIColor(red: 74/255.0, green: 21/255.0, blue: 75/255.0, alpha: 1.0)
            label.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 16).isActive = true
            label.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -16).isActive = true
            if (index == 0) {
                margins = goalsView.layoutMarginsGuide
                label.topAnchor.constraint(equalTo: margins.topAnchor, constant: 16).isActive = true
            } else {
                margins = viewArray[index-1].layoutMarginsGuide
                label.topAnchor.constraint(equalTo: margins.bottomAnchor, constant: 16).isActive = true
            }
            if (index == courseGoal.count - 1) {
                margins = goalsView.layoutMarginsGuide
                label.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -16).isActive = true
            }
            viewArray.append(label)
        }
    }
    
    // convert int level to string level
    func getLevelString(level: Int) -> String {
        switch level {
        case 1:
            return "Beginner"
        case 2:
            return "Intermediate"
        case 3:
            return "Advanced"
        default:
            return "Beginner"
        }
    }
    
    // enroll is clicked
    func enrollCourse() -> Void {
        UserDataStore.instance.courses?.append(course!.id)
        firebaseRef.updateChildValues([
            "/courses/\(course!.id)/attendantCount" : (course?.attendantCount ?? 0) + 1,
            "/users/\(UserDataStore.instance.uid!)/courses": UserDataStore.instance.courses ?? []
        ])
        enroll_button.isHidden = true
        enroll_button.isEnabled = false;
        enroll_button.isUserInteractionEnabled = false;
        continue_button.isHidden = false
        continue_button.isEnabled = true;
        continue_button.isUserInteractionEnabled = true;
        let vc = storyboard?.instantiateViewController(withIdentifier: "CongratsViewController") as? CongratsViewController
        vc?.urlString = urlString
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor(red: 74/255, green: 21/255, blue: 75/255, alpha: 1)
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    // continue is clicked 
    func continueCourse() -> Void {
        let vc = storyboard?.instantiateViewController(withIdentifier: "WebKitViewController") as? WebKitViewController
        vc?.urlString = urlString
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor(red: 74/255, green: 21/255, blue: 75/255, alpha: 1)
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
