//
//  CourseGoalTableViewCell.swift
//  Speakly
//  Course goal table vie cell
//  Created by iosdev on 04/05/2019.
//

import UIKit

class CourseGoalTableViewCell: UITableViewCell {

    @IBOutlet weak var courseGoalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
