//
//  CardView.swift
//  Speakly
//  Reusable card view logic
//  Created by iosdev on 26/04/2019.
//

import UIKit
import Cosmos

class CardView: UIView {
    
    
    @IBOutlet var cardView: UIView!
    @IBOutlet weak var cardView_image: UIImageView!
    @IBOutlet weak var cardView_title: UILabel!
    @IBOutlet weak var cardView_author: UILabel!
    @IBOutlet weak var cardView_price: UILabel!
    @IBOutlet weak var cardview_ratings: CosmosView!
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super .init(coder: aDecoder)
        commonInit()
    }
    
    // load card view and set border radius
    func commonInit() {
        Bundle.main.loadNibNamed("CardView", owner: self, options: nil)
        cardView.frame = self.bounds
        cardView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        cardView.layer.cornerRadius = 10
        cardView.layer.masksToBounds = true
        addSubview(cardView)
    }
    
}
