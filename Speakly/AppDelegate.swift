//
//  AppDelegate.swift
//  iOSTemplate
//
//  Created by John Hampton on 2/24/19.
//

import UIKit
import CoreData
import Firebase
import Localize_Swift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var handle: AuthStateDidChangeListenerHandle?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UITabUpdate()
        FirebaseApp.configure()
        handle = Auth.auth().addStateDidChangeListener { auth, user in
            let firebaseRef = Database.database().reference()
            if let uid = UserDataStore.instance.uid {
                firebaseRef.child("users/\(uid)").removeAllObservers()
            }
            UserDataStore.instance.email = user?.email
            UserDataStore.instance.uid = user?.uid
            UserDataStore.instance.username = user?.displayName
            if (UserDataStore.instance.uid != nil) {
                // set observer to update UserDataStore when user values in database are updated
                firebaseRef.child("users/\(UserDataStore.instance.uid!)").observe(DataEventType.value, with: {snapshot in
                    let userValue = snapshot.value as? NSDictionary
                    UserDataStore.instance.language = userValue?["language"] as? String ?? "en"
                    UserDataStore.instance.level = userValue?["level"] as? Int ?? 1
                    UserDataStore.instance.phone = userValue?["phone"] as? String ?? ""
                    UserDataStore.instance.courses = userValue?["courses"] as? [Int] ?? []
                    Localize.setCurrentLanguage(UserDataStore.instance.language!)
                })
            }
            if let signedInUser = user {
                firebaseRef.child("users").child(signedInUser.uid)
                    .child("level")
                    .observeSingleEvent(of: .value, with: { (snapshot) in
                        if snapshot.exists() {
                            self.setupRootViewController(viewController: TabBarViewController.storyboardInstance()!)
                        } else {
                            self.setupRootViewController(viewController: GetStartViewController.storyboardInstance()!)
                        }
                    })
                // Change this to HomeViewController when it is ready
                print("INFO: ", signedInUser.displayName ?? "no display name", signedInUser.email ?? "no email")
            } else {
                if UserDefaults.standard.bool(forKey: "hasViewOnboarding") {
                    self.setupRootViewController(viewController: LoginViewController.storyboardInstance()!)
                } else {
                    let storyboard = UIStoryboard(name: "OnboardingViewController", bundle: nil)
                    if let onboardingViewController = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController {
                        self.setupRootViewController(viewController: onboardingViewController)
                    }
                }
            }
        }
        
        return true
    }
    
    // tab bar UI 
    func UITabUpdate() {
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().tintColor = UIColor(red: 0.35, green: 0.71, blue: 0.51, alpha: 1.0)
        UITabBar.appearance().layer.shadowColor = UIColor.black.cgColor
        UITabBar.appearance().layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        UITabBar.appearance().layer.shadowRadius = 2
        UITabBar.appearance().layer.shadowOpacity = 0.2
    }
    
    // set root view controller
    private func setupRootViewController(viewController: UIViewController) {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        window?.rootViewController  = viewController
        window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if let handle = self.handle {
            Auth.auth().removeStateDidChangeListener(handle)
        }
    }

    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Speakly")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

