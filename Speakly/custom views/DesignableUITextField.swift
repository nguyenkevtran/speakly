//
//  File.swift
//  iOSTemplate
//  Customize UITextField UI
//  Created by Giang Pham on 22/04/2019.
//

import UIKit

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let textBounds = super.textRect(forBounds: bounds)
        let padding: UIEdgeInsets
        if (leftImage == nil) {
            padding = UIEdgeInsets(top: topPadding, left: leftPadding, bottom: bottomPadding, right: rightPadding)
        } else {
            padding = UIEdgeInsets(top: topPadding, left: 10, bottom: bottomPadding, right: rightPadding)
        }
        return textBounds.inset(by: padding)
    }
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let textBounds = super.textRect(forBounds: bounds)
        let padding: UIEdgeInsets
        if (leftImage == nil) {
            padding = UIEdgeInsets(top: topPadding, left: leftPadding, bottom: bottomPadding, right: rightPadding)
        } else {
            padding = UIEdgeInsets(top: topPadding, left: 10, bottom: bottomPadding, right: rightPadding)
        }
        return textBounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let textBounds = super.textRect(forBounds: bounds)
        let padding: UIEdgeInsets
        if (leftImage == nil) {
            padding = UIEdgeInsets(top: topPadding, left: leftPadding, bottom: bottomPadding, right: rightPadding)
        } else {
            padding = UIEdgeInsets(top: topPadding, left: 10, bottom: bottomPadding, right: rightPadding)
        }
        return textBounds.inset(by: padding)
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    @IBInspectable var topPadding: CGFloat = 0
    @IBInspectable var bottomPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: color])
    }
}

