//
//  CollectionViewCell.swift
//  Speakly
//  CollectionView swift file for home page
//  Created by iosdev on 30/04/2019.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardView: CardView!
    var course: Course? {
        didSet {
            updateUI()
        }
    }

    func updateUI() {
        cardView.cardView_author.text = course?.author
        cardView.cardView_price.text = course?.price
        cardView.cardView_title.text  = course?.title
        cardView.cardview_ratings.rating = course?.ratings ?? 0
        cardView.cardView_image.load(url: URL(string: (course?.image)!)!)
    }
}

// entend UIImageView for setting UIImage picture by url
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
