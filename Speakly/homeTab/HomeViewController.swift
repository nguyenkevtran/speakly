//
//  HomeViewController.swift
//  Speakly
//  HomeView swift file for home page logic
//  Created by iosdev on 26/04/2019.
//

import UIKit
import Firebase
import Localize_Swift

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var popularCourseLabel: UILabel!
    @IBOutlet weak var freeCourseLabel: UILabel!
    @IBOutlet weak var topPickCourseLabel: UILabel!
    @IBOutlet weak var viewAllCourse1: UIButton!
    @IBOutlet weak var viewAllCourse2: UIButton!
    @IBOutlet weak var popularCourse_collection: UICollectionView!
    @IBOutlet weak var freeCourse_collection: UICollectionView!
    @IBOutlet weak var topPickCourse_collection: UICollectionView!
    @IBOutlet weak var viewAllCourse3: UIButton!
    
    var popularCourseCollectionIdentifier = "PopularCourseCell"
    var freeCourseCollectionIdentifier = "FreeCourseCell"
    var topPickCourseCollectionIdentifier = "TopPickCourseCell"
    var firebaseRef: DatabaseReference!
    var popularCoursesArray: [Course] = []
    var freeCoursesArray: [Course] = []
    var topPickCoursesArray: [Course] = []
    var userLevel: Int?
    
    static func storyboardInstance() -> HomeViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? HomeViewController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        setText()
        // set width and height for UICollectionView Cell
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * 0.4)
        let cellHeight = cellWidth * 1.5
        let popularCourse_layout = popularCourse_collection!.collectionViewLayout as! UICollectionViewFlowLayout
        popularCourse_layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        let freeCourse_layout = freeCourse_collection!.collectionViewLayout as! UICollectionViewFlowLayout
        freeCourse_layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        let topPickCourse_layout = topPickCourse_collection!.collectionViewLayout as! UICollectionViewFlowLayout
        topPickCourse_layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        // set dataSource and delegate for UICollectionViews
        popularCourse_collection.delegate = self
        popularCourse_collection.dataSource = self
        freeCourse_collection.delegate = self
        freeCourse_collection.dataSource = self
        topPickCourse_collection.delegate = self
        topPickCourse_collection.dataSource = self
    }
    
    // create course's arrays from firebase data
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        if firebaseRef == nil {
            firebaseRef = Database.database().reference()
        }
        if UserDataStore.instance.level != nil && userLevel != UserDataStore.instance.level {
            userLevel = UserDataStore.instance.level!
            firebaseRef.child("courses").observeSingleEvent(of: .value, with: { (snapshot) in
                let allCourses = (snapshot.value as! NSArray).enumerated().map({ Course($1 as! Dictionary<String, AnyObject>, $0) }).filter({ $0.level <= self.userLevel ?? 1 })
                self.popularCoursesArray = allCourses.sorted(by: { $0.attendantCount > $1.attendantCount })
                self.freeCoursesArray = allCourses.filter({ $0.price == "Free" })
                self.topPickCoursesArray = allCourses.filter({ $0.ratings > 3 })
                
                DispatchQueue.main.async(execute: {
                    self.popularCourse_collection.reloadData()
                    self.topPickCourse_collection.reloadData()
                    self.freeCourse_collection.reloadData()
                })
            })
        }
    }
    
    // moving to catagory page 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! CategoryCollectionViewController
        if segue.identifier == "ViewPopular" {
            destinationVC.courseArray = popularCoursesArray
            destinationVC.sectionHeaderLabel = "Popular courses"
        } else if segue.identifier == "ViewFree" {
            destinationVC.courseArray = freeCoursesArray
            destinationVC.sectionHeaderLabel = "Free courses"
        } else {
            destinationVC.courseArray = topPickCoursesArray
            destinationVC.sectionHeaderLabel = "Top pick for you"
        }
    }
    
    @objc func setText() {()
        topPickCourseLabel.text = "Top Pick For You".localized()
        popularCourseLabel.text = "Popular Courses".localized()
        freeCourseLabel.text = "Free Courses".localized()
        viewAllCourse1.setTitle("View all course".localized(), for: .normal)
        viewAllCourse2.setTitle("View all course".localized(), for: .normal)
        viewAllCourse3.setTitle("View all course".localized(), for: .normal)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == self.popularCourse_collection) {
            return popularCoursesArray.count
        } else if (collectionView == self.freeCourse_collection) {
            return freeCoursesArray.count
        } else if (collectionView == self.topPickCourse_collection) {
            return topPickCoursesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (collectionView == self.popularCourse_collection) {
            return prepareCourseCell(collectionView: collectionView, identifier: popularCourseCollectionIdentifier, indexPath: indexPath, course: popularCoursesArray[indexPath.item])
        } else if (collectionView == self.freeCourse_collection) {
            return prepareCourseCell(collectionView: collectionView, identifier: freeCourseCollectionIdentifier, indexPath: indexPath, course: freeCoursesArray[indexPath.item])
        } else {
            return prepareCourseCell(collectionView: collectionView, identifier: topPickCourseCollectionIdentifier, indexPath: indexPath, course: topPickCoursesArray[indexPath.item])
        }
    }
    
    func prepareCourseCell(collectionView: UICollectionView, identifier: String, indexPath: IndexPath, course: Course) -> CollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CollectionViewCell
        // set shadow and corner radius for cardView
        cell.contentView.layer.cornerRadius = 10
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width:0,height: 2.0)
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
        cell.course = course
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor(red: 74/255, green: 21/255, blue: 75/255, alpha: 1)
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc!, animated: true)
        
        if (collectionView == self.popularCourse_collection) {
            vc?.course = popularCoursesArray[indexPath.item]
        } else if (collectionView == self.freeCourse_collection) {
            vc?.course = freeCoursesArray[indexPath.item]
        } else if (collectionView == self.topPickCourse_collection) {
            vc?.course = topPickCoursesArray[indexPath.item]
        }
    }
}
