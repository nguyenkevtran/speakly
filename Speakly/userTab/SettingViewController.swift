//
//  SettingViewController.swift
//  Speakly
//  SettingView swift file for user setting page's logic
//  Created by Phat Doan on 03/05/2019.
//

import UIKit
import Firebase
import Localize_Swift

class SettingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var myCourseCollection: UICollectionView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var selectLanguageLabel: UILabel!
    @IBOutlet weak var selectLanguageButton: UIButton!
    @IBOutlet weak var myCourseLabel: UILabel!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var selectLevelLabel: UIButton!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var courseEnrolledLabel: UILabel!
    @IBOutlet weak var selectLanguageView: UIView!
    @IBOutlet weak var selectLevelView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var user_username: UILabel!
    @IBOutlet weak var user_email: UILabel!
    @IBOutlet weak var user_courses_count: UILabel!
    @IBOutlet weak var language_button_label: UIButton!
    @IBOutlet weak var level_button_label: UIButton!
    let availableLanguages = Localize.availableLanguages()
    var myCourses = [Course]()
    var firebaseRef: DatabaseReference!
    var courseIDs: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        setText()
        avatarImageView.layer.cornerRadius = 10
        updateUI(sender: selectLanguageView)
        updateUI(sender: selectLevelView)
        updateUI(sender: changePasswordView)
        updateUI(sender: infoView)
        setupNavigationItem()
        myCourseCollection.delegate = self
        myCourseCollection.dataSource = self
        
        // bind user's info
        user_username.text = UserDataStore.instance.username
        user_email.text = UserDataStore.instance.email
        user_courses_count.text = String(UserDataStore.instance.courses?.count ?? 0)
        language_button_label.setTitle(Localize.displayNameForLanguage(Localize.currentLanguage()), for: UIControl.State.normal)
        level_button_label.setTitle(parseLevelToString(UserDataStore.instance.level ?? 1), for: UIControl.State.normal)
        
        // calculating cell size
        let screenSize = UIScreen.main.bounds.size
        let cellWidth = floor(screenSize.width * 0.4)
        let cellHeight = cellWidth * 1.5
        let popularCourse_layout = myCourseCollection!.collectionViewLayout as! UICollectionViewFlowLayout
        popularCourse_layout.itemSize = CGSize(width: cellWidth, height: cellHeight)
        
        // fetching user courses
        courseIDs = UserDataStore.instance.courses ?? []
        firebaseRef = Database.database().reference()
        if courseIDs.count > 0 {
            for id in courseIDs {
                firebaseRef.child("courses/\(id)").observeSingleEvent(of: .value, with: { (snapshot) in
                    self.myCourses.append(Course(snapshot.value as! Dictionary<String, AnyObject>, id))
                    print(self.myCourses)
                    DispatchQueue.main.async(execute: {
                        self.myCourseCollection.reloadData()
                    })
                })
            }
        }
    }
    
    // user setting page will appear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(setText), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        if courseIDs.count != UserDataStore.instance.courses?.count {
            user_courses_count.text = String(UserDataStore.instance.courses?.count ?? 0)
            courseIDs = UserDataStore.instance.courses ?? []
            myCourses = []
            if courseIDs.count > 0 {
                for id in courseIDs {
                    firebaseRef.child("courses/\(id)").observeSingleEvent(of: .value, with: { (snapshot) in
                        self.myCourses.append(Course(snapshot.value as! Dictionary<String, AnyObject>, id))
                        DispatchQueue.main.async(execute: {
                            self.myCourseCollection.reloadData()
                        })
                    })
                }
            }
        }
    }
    
    @objc func setText() {()
        selectLanguageLabel.text = "Language".localized()
        levelLabel.text = "Level".localized()
        myCourseLabel.text = "My Courses".localized()
        courseEnrolledLabel.text = "Course Enrolled".localized()
        selectLanguageButton.setTitle("Select language".localized(), for: .normal)
        selectLevelLabel.setTitle("Select level".localized(), for: .normal)
        changePasswordButton.setTitle("Change Password".localized(), for: .normal)
    }
    
    // change UI
    func updateUI(sender: UIView!) {
        sender.layer.cornerRadius = 10
        sender.layer.shadowOffset = .zero
        sender.layer.shadowRadius = 5
        sender.layer.shadowColor = UIColor.black.cgColor
        sender.layer.shadowOpacity = 0.08
    }
    
    // log out button for navigation bar
    func setupNavigationItem() {
        let logoutButton = UIButton(type: .system)
        logoutButton.setImage(UIImage(named: "logout")?.withRenderingMode(.alwaysOriginal), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logoutButton)
        logoutButton.addTarget(self, action: #selector(logoutButtonClicked(_ :)), for: .touchUpInside)
        self.view.addSubview(logoutButton)
    }
    
    // convert int level to string level
    func parseLevelToString(_ level: Int) -> String {
        switch level {
        case 1:
            return "Beginner".localized()
        case 2:
            return "Intermediate".localized()
        case 3:
            return "Advanced".localized()
        default:
            return "Beginner".localized()
        }
    }
    
    // log out button is clicked
    @objc func logoutButtonClicked(_ : UIButton!) {
        let alert = UIAlertController(title: "Logout".localized(), message: "Are you sure you want to logout?".localized(), preferredStyle: .alert)
        let alertCancel = UIAlertAction(title: "No".localized(), style: .cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        let alertOK = UIAlertAction(title: "Yes".localized(), style: .default, handler: {
            (alert: UIAlertAction) -> Void in
            do {
                try Auth.auth().signOut()
            } catch let err {
                print(err)
            }
        })
        alert.addAction(alertCancel)
        alert.addAction(alertOK)
        present(alert, animated: true, completion: nil)
    }
    
    // switch language button
    @IBAction func switchLanguage(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: ("Switch Language").localized(), preferredStyle: UIAlertController.Style.actionSheet)
        for language in availableLanguages {
            let displayName = Localize.displayNameForLanguage(language)
            let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.firebaseRef.updateChildValues(["users/\(UserDataStore.instance.uid!)/language": language]) {
                    (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        print("Data could not be saved: \(error).")
                    } else {
                        self.language_button_label.setTitle(Localize.displayNameForLanguage(Localize.currentLanguage()), for: UIControl.State.normal)
                        print("Data saved successfully!")
                    }
                }
            })
            actionSheet.addAction(languageAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // change language button
    @IBAction func selectLevelTapped(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: ("We will suggest courses based on your level").localized(), preferredStyle: UIAlertController.Style.actionSheet)
        for level in [1,2,3] {
            let displayName = self.parseLevelToString(level)
            let levelSelectAction = UIAlertAction(title: displayName, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                self.firebaseRef.updateChildValues(["users/\(UserDataStore.instance.uid!)/level": level]) {
                    (error:Error?, ref:DatabaseReference) in
                    if let error = error {
                        print("Data could not be saved: \(error).")
                    } else {
                        self.level_button_label.setTitle(self.parseLevelToString(UserDataStore.instance.level ?? 1), for: UIControl.State.normal)
                        print("Data saved successfully!")
                    }
                }
            })
            actionSheet.addAction(levelSelectAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // change password button
    @IBAction func changePasswordRequest(_ sender: Any) {
        if let email = UserDataStore.instance.email {
            Auth.auth().sendPasswordReset(withEmail: email) { error in
                if error != nil {
                    let alert = UIAlertController(title: "Error", message: "Request for password change failed", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { _ in
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Announcement", message: "A link to reset password has been sent to your email address.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myCourses.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCourseCollectionViewCell", for: indexPath) as! MyCourseCollectionViewCell
        cell.course = myCourses[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
        let backItem = UIBarButtonItem()
        backItem.tintColor = UIColor(red: 74/255, green: 21/255, blue: 75/255, alpha: 1)
        navigationItem.backBarButtonItem = backItem
        vc?.course = myCourses[indexPath.item]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
