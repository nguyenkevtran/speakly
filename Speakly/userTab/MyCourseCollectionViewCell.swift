//
//  MyCourseCollectionViewCell.swift
//  Speakly
//
//  Created by iosdev on 06/05/2019.
//

import UIKit

class MyCourseCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: CardView!
    var course: Course? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        cardView.cardView_author.text = course?.author
        cardView.cardView_price.text = course?.price
        cardView.cardView_title.text  = course?.title
        cardView.cardview_ratings.rating = course?.ratings ?? 0
        cardView.cardView_image.load(url: URL(string: (course?.image)!)!)
    }
}
