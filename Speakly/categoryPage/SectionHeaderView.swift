//
//  SectionHeaderView.swift
//  Speakly
//  Header view for Category collection view 
//  Created by iosdev on 02/05/2019.
//

import UIKit

class SectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var categoryLabel: UILabel!
    
}
