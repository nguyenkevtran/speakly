//
//  CategoryCollectionViewCell.swift
//  Speakly
//  Handle category collection view's cell logic
//  Created by iosdev on 02/05/2019.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cardView: CardView!
    var course: Course? {
        didSet {
            updateUI()
        }
    }
    
    // set cardview's display
    func updateUI() {
        cardView.cardView_author.text = course?.author
        cardView.cardView_price.text = course?.price
        cardView.cardView_title.text  = course?.title
        cardView.cardview_ratings.rating = course?.ratings ?? 0
        cardView.cardView_image.load(url: URL(string: (course?.image)!)!)
    }
}
