//
//  SignUpViewController.swift
//  Speakly
//  Sign up view page' logic
//  Created by Giang Pham on 25/04/2019.
//

import UIKit
import Firebase

class SignUpViewController: BaseViewController {
    
    static func storyboardInstance() -> SignUpViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? SignUpViewController
    }

    @IBOutlet weak var usernameTextField: DesignableUITextField!
    @IBOutlet weak var emailTextField: DesignableUITextField!
    @IBOutlet weak var passwordTextField: DesignableUITextField!
    @IBOutlet weak var signUpButton: UIButton!
    
    // change to login page
    @IBAction func moveBackToLogin(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // sign up buton action
    @IBAction func signUp(_ sender: UIButton) {
        guard let email = emailTextField.text, let password = passwordTextField.text, let username = usernameTextField.text else {
            return
        }
        //check if email is valid 
        if email.isValidEmail() {
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] authResult, error in
                if (error != nil) {
                    print("ERROR: Fail to create user", error!)
                    return
                } else {
                    let changeNameRequest = authResult?.user.createProfileChangeRequest()
                    changeNameRequest?.displayName = username
                    changeNameRequest?.commitChanges(completion: { (err) in
                        if err == nil {
                            UserDataStore.instance.username = username
                            self?.dismiss(animated: true, completion: nil)
                        } else {
                            print("ERROR: Fail to change display name", err!)
                        }
                    })
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func emailChanged(_ sender: UITextField) {
        signUpButton.isEnabled = shouldEnableSignUpButton()
    }
    
    @IBAction func passwordChanged(_ sender: UITextField) {
        signUpButton.isEnabled = shouldEnableSignUpButton()
    }
    
    @IBAction func usernameChanged(_ sender: UITextField) {
        signUpButton.isEnabled = shouldEnableSignUpButton()
    }
    
    func shouldEnableSignUpButton() -> Bool {
        return !(emailTextField.text?.isEmpty ?? true || passwordTextField.text?.isEmpty ?? true || usernameTextField.text?.isEmpty ?? true)
    }
}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
