//
//  GetStartViewController.swift
//  Speakly
//  GetstartView swift file to handle all of the logic of get started page
//  Created by Giang Pham on 01/05/2019.
//

import UIKit
import Firebase

class GetStartViewController: UIViewController {

    static func storyboardInstance() -> GetStartViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? GetStartViewController
    }
    
    @IBOutlet weak var beginner: UIButton!
    @IBOutlet weak var intermediate: UIButton!
    @IBOutlet weak var advanced: UIButton!
    
    private var level: Int = 1
    
    // present tab bar 
    @IBAction func getStart(_ sender: UIButton) {
        present(TabBarViewController.storyboardInstance()!, animated: true, completion: nil)
        Database.database().reference().child("users/\(UserDataStore.instance.uid!)/level").setValue(level)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for button in [beginner, intermediate, advanced] {
            button?.titleLabel?.minimumScaleFactor = 0.5
            button?.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    
    // choosing level
    @IBAction func beginnerTap(_ sender: UIButton) {
        changeColor(sender)
        level = 1
    }
    
    // choosing level
    @IBAction func intermediateTap(_ sender: UIButton) {
        changeColor(sender)
        level = 2
    }
    
    // choosing level
    @IBAction func advancedTap(_ sender: UIButton) {
        changeColor(sender)
        level = 3
    }
    
    // change level buttons' color
    private func changeColor(_ sender: UIButton) {
        for button in [beginner, intermediate, advanced] {
            if button == sender {
                button?.backgroundColor = #colorLiteral(red: 0.2567885518, green: 0.7775918841, blue: 0.9455396533, alpha: 1)
                button?.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            } else {
                button?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                button!.setTitleColor(#colorLiteral(red: 0.2903772891, green: 0.08184266835, blue: 0.2946359217, alpha: 1), for: .normal)
            }
        }
    }
}
